package com.activation.activationuser.exceptionhandler;

import com.activation.activationuser.responsebody.ExceptionResponseBody;
import com.activation.activationuser.responsebody.abstraction.AbstractResponseBody;
import com.activation.activationuser.responsebody.validation.ValidationExceptionResponseBody;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;


@ControllerAdvice
public class ControllerExceptionHandler {


    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<AbstractResponseBody> handleException(MethodArgumentNotValidException exception) {
        AbstractResponseBody response = new ValidationExceptionResponseBody(exception);
        return new ResponseEntity<>(response, response.getStatus());
    }

    @ExceptionHandler(InvalidDataAccessApiUsageException.class)
    public ResponseEntity<AbstractResponseBody> handleException(InvalidDataAccessApiUsageException exception) {
        AbstractResponseBody response = new ValidationExceptionResponseBody(exception);
        return new ResponseEntity<>(response, response.getStatus());
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<AbstractResponseBody> handleException(HttpMessageNotReadableException exception) {
        AbstractResponseBody response = new ExceptionResponseBody(exception);
        return new ResponseEntity<>(response, response.getStatus());
    }


}

package com.activation.activationuser.aspect;


import com.activation.activationuser.dto.UserEmailDTO;
import com.activation.activationuser.responsebody.abstraction.AbstractResponseBody;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;


@Aspect
@Component
public class LoggingControllerAspect {

    private final Logger log = LogManager.getLogger(this.getClass());

    @Pointcut("execution(* com.activation.activationuser.controller.ActivationController.*(*)) && args(userEmailDTO)")
    public void activateUserAtControllerPointCut(UserEmailDTO userEmailDTO) { }


    @Before(value = "activateUserAtControllerPointCut(userEmailDTO)", argNames = "userEmailDTO")
    public void aroundActivateUserMethod(UserEmailDTO userEmailDTO){
        log.info(String.format("Почта: %s - попытка активации ",
                userEmailDTO.getEmail()
        ));
    }


    @AfterReturning(value = "activateUserAtControllerPointCut(userEmailDTO)",returning = "response", argNames = "userEmailDTO,response")
    public void afterActivateUserMethod(UserEmailDTO userEmailDTO, ResponseEntity<AbstractResponseBody> response){
        log.info(String.format("Почта: %s - %s",
                userEmailDTO.getEmail(),
                response.getStatusCode().value() == 200 ? "успешно активирована" : "была отклонёна"
        ));
    }



}

package com.activation.activationuser.controller;

import com.activation.activationuser.dto.UserDTO;
import com.activation.activationuser.responsebody.abstraction.AbstractResponseBody;
import org.springframework.http.ResponseEntity;



public interface NotificationController {

    ResponseEntity<AbstractResponseBody> notifyUser(UserDTO user);

}

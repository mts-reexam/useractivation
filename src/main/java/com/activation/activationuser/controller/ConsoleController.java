package com.activation.activationuser.controller;

import com.activation.activationuser.dto.UserDTO;
import com.activation.activationuser.feignclient.NotificationUserFeignClient;
import com.activation.activationuser.responsebody.abstraction.AbstractResponseBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ConsoleController implements NotificationController{

    private final NotificationUserFeignClient feignClient;

    @Autowired
    public ConsoleController(NotificationUserFeignClient feignClient) {
        this.feignClient = feignClient;
    }

    @Override
    public ResponseEntity<AbstractResponseBody> notifyUser(UserDTO user) {
        return feignClient.notifyUserByConsole(user);
    }
}

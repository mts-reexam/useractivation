package com.activation.activationuser.controller;

import com.activation.activationuser.dto.UserEmailDTO;
import com.activation.activationuser.responsebody.abstraction.AbstractResponseBody;
import com.activation.activationuser.service.activationservice.ActivateUserService;
import com.activation.activationuser.service.activationservice.ActivateUserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/activation/v0")
public class ActivationController {

    private final ActivateUserService activateUserService;

    @Autowired
    public ActivationController(ActivateUserServiceImpl activateUserService) {
        this.activateUserService = activateUserService;
    }

    @PutMapping(value = "/users")
    @ResponseBody
    public ResponseEntity<AbstractResponseBody> activateUser(@Valid @RequestBody UserEmailDTO email) {
        AbstractResponseBody response = activateUserService.activateUser(email);
        return new ResponseEntity<>(response, response.getStatus());
    }




}

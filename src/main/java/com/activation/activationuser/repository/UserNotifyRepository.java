package com.activation.activationuser.repository;

import com.activation.activationuser.entity.UserNotifyEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserNotifyRepository extends JpaRepository<UserNotifyEntity, String> {
}

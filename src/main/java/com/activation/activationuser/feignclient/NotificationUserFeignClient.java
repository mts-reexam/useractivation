package com.activation.activationuser.feignclient;


import com.activation.activationuser.dto.UserDTO;
import com.activation.activationuser.responsebody.abstraction.AbstractResponseBody;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name = "notification", url = "${feign.config.url}")
public interface NotificationUserFeignClient {


    @PostMapping(value = "/email/users")
    ResponseEntity<AbstractResponseBody> notifyUserByEmail(UserDTO user);

    @PostMapping(value = "/console/users")
    ResponseEntity<AbstractResponseBody> notifyUserByConsole(UserDTO user);
}

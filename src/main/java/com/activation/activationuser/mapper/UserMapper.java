package com.activation.activationuser.mapper;

import com.activation.activationuser.dto.UserDTO;
import com.activation.activationuser.entity.UserEntity;
import com.activation.activationuser.entity.UserNotifyEntity;
import org.springframework.stereotype.Component;


@Component
public class UserMapper {

    public UserNotifyEntity mapUser_ToNotifyEntity(UserDTO user) {
        return new UserNotifyEntity(
                user.getEmail(),
                user.getUsername()
        );
    }

    public UserNotifyEntity mapUserEntity_ToUserNotifyEntity(UserEntity entity) {
        return new UserNotifyEntity(
                entity.getEmail(),
                entity.getUsername()
        );
    }

    public UserDTO mapNotifyEntity_ToUser(UserNotifyEntity entity) {
        return new UserDTO(
                entity.getUsername(),
                entity.getEmail());
    }


}

package com.activation.activationuser.exception;

public class UserAlreadyActivatedException extends Exception {


    public UserAlreadyActivatedException(String email) {
        super(String.format("Пользователь с таким адресом электронной почты (%s) уже активирован!",email));
    }
}

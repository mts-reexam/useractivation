package com.activation.activationuser.exception;

public class NoUserInDbException extends Exception {

    public NoUserInDbException(String email) {
        super(String.format("Пользователь с таким адресом электронной почты (%s) не найден", email));
    }
}

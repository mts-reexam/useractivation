package com.activation.activationuser.dto;


import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Getter;
import lombok.NonNull;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;
import java.util.Locale;

@Getter
public class UserEmailDTO {

    @NonNull
    @Email(regexp = "[a-zA-Z0-9]+[@]{1}+[a-zA-Z0-9]+[.]{1}+[a-zA-Z]+")
    @Size(max = 256)
    private final String email;

    @JsonCreator
    public UserEmailDTO(String email) {
        this.email = email.toLowerCase(Locale.ROOT);
    }


}

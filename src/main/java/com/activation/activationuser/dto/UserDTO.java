package com.activation.activationuser.dto;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

@Getter
@AllArgsConstructor
public class UserDTO {

    @NonNull
    @Size(min = 2, max = 20)
    private String username;

    @NonNull
    @Email(regexp = "[a-zA-Z0-9]+[@]{1}+[a-zA-Z0-9]+[.]{1}+[a-zA-Z]+")
    @Size(max = 256)
    private String email;


}

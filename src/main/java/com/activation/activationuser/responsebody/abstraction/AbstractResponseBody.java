package com.activation.activationuser.responsebody.abstraction;


import com.activation.activationuser.responsebody.ExceptionResponseBody;
import com.activation.activationuser.responsebody.SuccessResponseBody;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Getter;
import org.springframework.http.HttpStatus;

import java.sql.Timestamp;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.EXISTING_PROPERTY,
        property = "status"
)
@JsonSubTypes({
        @JsonSubTypes.Type(value = SuccessResponseBody.class, name = "OK"),
        @JsonSubTypes.Type(value = ExceptionResponseBody.class, name = "INTERNAL_SERVER_ERROR"),
})

@Getter
public abstract class AbstractResponseBody {

    protected Timestamp timestamp;

    protected HttpStatus status;

    @JsonDeserialize(using = TraceDeserializer.class)
    protected AbstractTrace trace;

}

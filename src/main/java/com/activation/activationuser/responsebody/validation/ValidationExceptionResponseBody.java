package com.activation.activationuser.responsebody.validation;


import com.activation.activationuser.responsebody.abstraction.AbstractResponseBody;
import com.activation.activationuser.responsebody.trace.ListTrace;
import com.activation.activationuser.responsebody.trace.StringTrace;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.stream.Collectors;

public class ValidationExceptionResponseBody extends AbstractResponseBody {


    public ValidationExceptionResponseBody(MethodArgumentNotValidException exception){
        this.timestamp = Timestamp.valueOf(LocalDateTime.now());
        this.status = HttpStatus.BAD_REQUEST;
        this.trace = new ListTrace(exception.getBindingResult().getFieldErrors().stream()
                .map(error -> new ErrorMessage(error.getField(),error.getRejectedValue(), error.getDefaultMessage()))
                .collect(Collectors.toList()));
    }

    public ValidationExceptionResponseBody(InvalidDataAccessApiUsageException exception) {

        this.timestamp = Timestamp.valueOf(LocalDateTime.now());
        this.status = HttpStatus.BAD_REQUEST;
        this.trace = new StringTrace( "Поле не может содержать null");

    }





}


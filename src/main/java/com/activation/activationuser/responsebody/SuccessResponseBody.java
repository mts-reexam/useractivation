package com.activation.activationuser.responsebody;


import com.activation.activationuser.responsebody.abstraction.AbstractResponseBody;
import com.activation.activationuser.responsebody.trace.StringTrace;
import org.springframework.http.HttpStatus;

import java.sql.Timestamp;
import java.time.LocalDateTime;

public class SuccessResponseBody extends AbstractResponseBody {

    public SuccessResponseBody() {
        this.status = HttpStatus.OK;
        this.timestamp = Timestamp.valueOf(LocalDateTime.now());
        this.trace = new StringTrace( "Успешный запрос на активацию");
    }
}

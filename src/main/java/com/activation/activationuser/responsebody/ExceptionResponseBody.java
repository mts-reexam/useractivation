package com.activation.activationuser.responsebody;

import com.activation.activationuser.exception.NoUserInDbException;
import com.activation.activationuser.exception.UserAlreadyActivatedException;
import com.activation.activationuser.responsebody.abstraction.AbstractResponseBody;
import com.activation.activationuser.responsebody.trace.StringTrace;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;

import java.sql.Timestamp;
import java.time.LocalDateTime;

public class ExceptionResponseBody extends AbstractResponseBody {


    public ExceptionResponseBody(NoUserInDbException exception) {
        this.timestamp = Timestamp.valueOf(LocalDateTime.now());
        this.status = HttpStatus.NOT_FOUND;
        this.trace = new StringTrace(exception.getMessage());
    }

    public ExceptionResponseBody(UserAlreadyActivatedException exception) {
        this.timestamp = Timestamp.valueOf(LocalDateTime.now());
        this.status = HttpStatus.CONFLICT;
        this.trace = new StringTrace(exception.getMessage());
    }

    public ExceptionResponseBody() {
        this.timestamp = Timestamp.valueOf(LocalDateTime.now());
        this.status = HttpStatus.CONFLICT;
        this.trace = new StringTrace("Неизвественая ошибка");
    }

    public ExceptionResponseBody(HttpMessageNotReadableException exception){

        this.timestamp = Timestamp.valueOf(LocalDateTime.now());
        this.status = HttpStatus.BAD_REQUEST;
        this.trace = new StringTrace("Возможно одно из полей содержит null");

    }

}

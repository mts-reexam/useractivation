package com.activation.activationuser.responsebody.trace;


import com.activation.activationuser.responsebody.abstraction.AbstractTrace;
import lombok.AllArgsConstructor;
import lombok.Getter;


@AllArgsConstructor
@Getter
public class StringTrace extends AbstractTrace {

    public String message;
}

package com.activation.activationuser.responsebody.trace;

import com.activation.activationuser.responsebody.abstraction.AbstractTrace;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;


@Getter
@AllArgsConstructor
public class ListTrace extends AbstractTrace {

    protected List<?> messages;
}

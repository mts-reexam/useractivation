package com.activation.activationuser.service.activationservice;

import com.activation.activationuser.dto.UserEmailDTO;
import com.activation.activationuser.entity.UserEntity;
import com.activation.activationuser.exception.NoUserInDbException;
import com.activation.activationuser.exception.UserAlreadyActivatedException;
import com.activation.activationuser.repository.UserRepository;
import com.activation.activationuser.responsebody.ExceptionResponseBody;
import com.activation.activationuser.responsebody.SuccessResponseBody;
import com.activation.activationuser.responsebody.abstraction.AbstractResponseBody;
import com.activation.activationuser.service.notificationservice.NotificationUserService;
import com.activation.activationuser.service.notificationservice.NotificationUserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActivateUserServiceImpl implements ActivateUserService {

    private final UserRepository userRepository;
    private final NotificationUserService notificationService;

    @Autowired
    public ActivateUserServiceImpl(UserRepository userRepository, NotificationUserServiceImpl notificationUserService) {
        this.userRepository = userRepository;
        this.notificationService = notificationUserService;
    }


    @Override
    public AbstractResponseBody activateUser(UserEmailDTO email) {
        try {
            if (!userRepository.existsById(email.getEmail())) {
                throw new NoUserInDbException(email.getEmail());
            }
            if (userRepository.findById(email.getEmail()).get().isUserActivated()) {
                throw new UserAlreadyActivatedException(email.getEmail());
            }
            UserEntity entity = userRepository.findById(email.getEmail()).get();
            entity.activateUser();
            userRepository.save(entity);
            notificationService.addUserToDataBase(entity);
        } catch (NoUserInDbException e) {
            return new ExceptionResponseBody(e);
        } catch (UserAlreadyActivatedException e) {
            return new ExceptionResponseBody(e);
        }
        return new SuccessResponseBody();
    }


}

package com.activation.activationuser.service.activationservice;

import com.activation.activationuser.dto.UserEmailDTO;
import com.activation.activationuser.responsebody.abstraction.AbstractResponseBody;

public interface ActivateUserService {


    AbstractResponseBody activateUser(UserEmailDTO email);
}

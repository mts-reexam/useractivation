package com.activation.activationuser.service.runnable;

import com.activation.activationuser.controller.ConsoleController;
import com.activation.activationuser.controller.NotificationController;
import com.activation.activationuser.dto.UserDTO;
import com.activation.activationuser.entity.UserNotifyEntity;
import com.activation.activationuser.repository.UserNotifyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
public class RunnableFactory {

    private final NotificationController controller;
    private final UserNotifyRepository userNotifyRepository;

    @Autowired
    public RunnableFactory(ConsoleController controller, UserNotifyRepository userNotifyRepository) {
        this.controller = controller;
        this.userNotifyRepository = userNotifyRepository;
    }

    @Bean
    @Scope(BeanDefinition.SCOPE_PROTOTYPE)
    @Lazy
    public NotificationRunnable makeNewRunnable(UserNotifyEntity entity, UserDTO user) {
        return new NotificationRunnable(userNotifyRepository, controller, entity, user);
    }
}

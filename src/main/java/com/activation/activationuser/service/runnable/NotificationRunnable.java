package com.activation.activationuser.service.runnable;

import com.activation.activationuser.controller.NotificationController;
import com.activation.activationuser.dto.UserDTO;
import com.activation.activationuser.entity.UserNotifyEntity;
import com.activation.activationuser.repository.UserNotifyRepository;
import com.activation.activationuser.responsebody.abstraction.AbstractResponseBody;
import org.springframework.http.ResponseEntity;

import java.util.Objects;


public class NotificationRunnable implements Runnable {
    private final NotificationController controller;
    private final UserNotifyRepository userNotifyRepository;
    private final UserNotifyEntity entity;
    private final UserDTO user;


    public NotificationRunnable(UserNotifyRepository userNotifyRepository, NotificationController controller, UserNotifyEntity entity, UserDTO user) {
        this.userNotifyRepository = userNotifyRepository;
        this.controller = controller;
        this.entity = entity;
        this.user = user;
    }


    @Override
    public void run() {
        while (userNotifyRepository.existsById(entity.getEmail())) {

            try {
                ResponseEntity<AbstractResponseBody> response = controller.notifyUser(user);
                if (Objects.requireNonNull(response.getBody()).getStatus().value() == 200) {
                    userNotifyRepository.delete(entity);
                    break;
                }
            } catch (Exception ignored) {
            }

        }

    }


}

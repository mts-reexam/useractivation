package com.activation.activationuser.service.notificationservice;

import com.activation.activationuser.entity.UserEntity;

public interface NotificationUserService {

    void addUserToDataBase(UserEntity entity);
}

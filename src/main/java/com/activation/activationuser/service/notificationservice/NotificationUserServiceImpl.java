package com.activation.activationuser.service.notificationservice;


import com.activation.activationuser.entity.UserEntity;
import com.activation.activationuser.mapper.UserMapper;
import com.activation.activationuser.repository.UserNotifyRepository;
import com.activation.activationuser.service.runnable.RunnableFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
public class NotificationUserServiceImpl implements NotificationUserService {

    private final UserNotifyRepository repository;

    private final UserMapper mapper;

    private final RunnableFactory factory;

    private final ExecutorService executor;

    @Autowired
    public NotificationUserServiceImpl(UserNotifyRepository userNotifyRepository,
                                       UserMapper mapper,
                                       RunnableFactory factory) {
        this.repository = userNotifyRepository;
        this.mapper = mapper;
        this.factory = factory;
        this.executor = Executors.newFixedThreadPool(10);
    }


    public void addUserToDataBase(UserEntity entity) {
        repository.save(mapper.mapUserEntity_ToUserNotifyEntity(entity));
    }

    @Scheduled(fixedRate = 5000)
    public void submitRunnable() {
        repository.findAll(Pageable.ofSize(10))
                .forEach(entity -> executor.submit(
                        factory.makeNewRunnable(entity, mapper.mapNotifyEntity_ToUser(entity)))
                );

    }


}

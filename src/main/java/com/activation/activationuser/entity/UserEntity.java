package com.activation.activationuser.entity;

import lombok.Getter;
import org.springframework.lang.NonNull;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Getter
@Table(name = "registered_users")
public class UserEntity {

    @NotNull
    @Column(name = "username")
    private String username;

    @NonNull
    @Id
    @Column(name = "email")
    private String email;

    @Column(name = "activated")
    private boolean isActivated;

    public void activateUser() {
        this.isActivated = true;
    }

    public boolean isUserActivated() {
        return this.isActivated;
    }

}

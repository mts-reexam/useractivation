package com.activation.activationuser.entity;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.lang.NonNull;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Table(name = "list_to_notify_users")
public class UserNotifyEntity {

    @NonNull
    @Id
    @Column(name = "email")
    private String email;

    @NotNull
    @Column(name = "username")
    private String username;

}

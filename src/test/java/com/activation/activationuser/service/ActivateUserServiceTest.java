package com.activation.activationuser.service;


import com.activation.activationuser.dto.UserEmailDTO;
import com.activation.activationuser.entity.UserEntity;
import com.activation.activationuser.repository.UserRepository;
import com.activation.activationuser.responsebody.ExceptionResponseBody;
import com.activation.activationuser.responsebody.SuccessResponseBody;
import com.activation.activationuser.responsebody.abstraction.AbstractResponseBody;
import com.activation.activationuser.service.activationservice.ActivateUserServiceImpl;
import com.activation.activationuser.service.notificationservice.NotificationUserServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static com.activation.activationuser.util.MockUser.mockUserEmail;
import static com.activation.activationuser.util.MockUser.mockUserEntity;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ActivateUserServiceTest {


    @Mock
    private UserRepository userRepository;

    @Mock
    private NotificationUserServiceImpl notificationService;

    @InjectMocks
    private ActivateUserServiceImpl activateUserService;



    @Test
    void activateUser_returnSuccessResponse_Test() throws IllegalAccessException {
        UserEmailDTO email = mockUserEmail();
        UserEntity entity = mockUserEntity();

        when(userRepository.existsById(email.getEmail())).thenReturn(true);
        when(userRepository.findById(email.getEmail())).thenReturn(Optional.of(entity));

        AbstractResponseBody response = activateUserService.activateUser(email);

        verify(userRepository).existsById(email.getEmail());
        verify(userRepository,times(2)).findById(email.getEmail());
        verify(notificationService).addUserToDataBase(entity);

        assertTrue(response instanceof SuccessResponseBody);
    }

    @Test
    void activateUser_returnExceptionResponse_NoUserInDb_Test() {
        UserEmailDTO email = mockUserEmail();

        when(userRepository.existsById(email.getEmail())).thenReturn(false);

        AbstractResponseBody response = activateUserService.activateUser(email);
        assertTrue(response instanceof ExceptionResponseBody);

        verify(userRepository,times(1)).existsById(email.getEmail());
        verify(userRepository,times(0)).findById(email.getEmail());

    }
    @Test
    void activateUser_returnExceptionResponse_AlreadyActivatedUser_Test() throws  IllegalAccessException {
        UserEmailDTO email = mockUserEmail();
        UserEntity entity = mockUserEntity();

        when(userRepository.existsById(email.getEmail())).thenReturn(true);
        when(userRepository.findById(email.getEmail())).thenReturn(Optional.of(entity));

        AbstractResponseBody response = activateUserService.activateUser(email);
        assertTrue(response instanceof ExceptionResponseBody);

        verify(userRepository,times(1)).existsById(email.getEmail());
        verify(userRepository,times(1)).findById(email.getEmail());

    }




}

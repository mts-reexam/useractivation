package com.activation.activationuser.service;


import com.activation.activationuser.controller.ConsoleController;
import com.activation.activationuser.dto.UserDTO;
import com.activation.activationuser.entity.UserEntity;
import com.activation.activationuser.entity.UserNotifyEntity;
import com.activation.activationuser.mapper.UserMapper;
import com.activation.activationuser.repository.UserNotifyRepository;
import com.activation.activationuser.service.notificationservice.NotificationUserServiceImpl;
import com.activation.activationuser.service.runnable.NotificationRunnable;
import com.activation.activationuser.service.runnable.RunnableFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;

import static com.activation.activationuser.util.MockUser.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class NotificationUserServiceTest {

    @Mock
    private ConsoleController notificationController;

    @Mock
    private UserNotifyRepository userNotifyRepository;

    @Mock
    private UserMapper mapper;

    @Mock
    private RunnableFactory factory;

    @InjectMocks
    private NotificationUserServiceImpl notificationUserService;


    private UserDTO user;

    private UserNotifyEntity userNotifyEntity;

    private UserEntity userEntity;

    @BeforeEach
    void mock() throws IllegalAccessException {
        user = mockUser();
        userNotifyEntity = mockUserNotifyEntity();
        userEntity = mockUserEntity();
    }

    @Test
    void addUserToQueue_CorrectUser_Test() {
        when(mapper.mapUserEntity_ToUserNotifyEntity(any(UserEntity.class))).thenReturn(userNotifyEntity);
        when(userNotifyRepository.save(userNotifyEntity)).thenReturn(userNotifyEntity);
        notificationUserService.addUserToDataBase(userEntity);

        verify(mapper,times(1)).mapUserEntity_ToUserNotifyEntity(userEntity);
        verify(userNotifyRepository,times(1)).save(userNotifyEntity);
    }

    @Test
    void submitRunnable_Test() {
        ArrayList<UserNotifyEntity> listFromRepo = fillList(userNotifyEntity);

        when(mapper.mapNotifyEntity_ToUser(any(UserNotifyEntity.class))).thenReturn(user);
        when(userNotifyRepository.findAll(Pageable.ofSize(10))).thenReturn(new PageImpl<>(listFromRepo));

        when(factory.makeNewRunnable(userNotifyEntity,user))
                .thenReturn(new NotificationRunnable(userNotifyRepository,notificationController,userNotifyEntity,user));

        notificationUserService.submitRunnable();


        verify(factory,times(10)).makeNewRunnable(userNotifyEntity,user);

    }


}

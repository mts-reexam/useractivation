package com.activation.activationuser;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("develop")
class ActivationUserApplicationTests {


    @Test
    void contextLoads() {

    }

}

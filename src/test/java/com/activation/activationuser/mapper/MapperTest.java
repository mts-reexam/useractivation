package com.activation.activationuser.mapper;


import com.activation.activationuser.dto.UserDTO;
import com.activation.activationuser.entity.UserEntity;
import com.activation.activationuser.entity.UserNotifyEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import static com.activation.activationuser.util.MockUser.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(classes = {UserMapper.class})
@ActiveProfiles("develop")
public class MapperTest {

    @Autowired
    UserMapper userMapper;

    private final UserDTO user = mockUser();

    private final UserNotifyEntity notifyEntity = mockUserNotifyEntity();

    private final UserEntity userEntity;

    {
        try {
            userEntity = mockUserEntity();
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }


    @Test
    void mapUser_ToNotifyEntity_Test() {
        UserNotifyEntity test = userMapper.mapUser_ToNotifyEntity(user);

        assertEquals(test.getEmail(),user.getEmail());
        assertEquals(test.getUsername(),user.getUsername());
    }
    @Test
    void mapNotifyEntity_ToUser_Test() {
        UserDTO test = userMapper.mapNotifyEntity_ToUser(notifyEntity);


        assertEquals(test.getEmail(),notifyEntity.getEmail());
        assertEquals(test.getUsername(),notifyEntity.getUsername());

    }

    @Test
    void mapUserEntity_ToUserNotifyEntity_Test() {
        UserNotifyEntity test = userMapper.mapUserEntity_ToUserNotifyEntity(userEntity);

        assertEquals(test.getEmail(),user.getEmail());
        assertEquals(test.getUsername(),user.getUsername());



    }




}

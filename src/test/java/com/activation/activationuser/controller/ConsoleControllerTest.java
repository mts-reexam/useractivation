package com.activation.activationuser.controller;


import com.activation.activationuser.dto.UserDTO;
import com.activation.activationuser.feignclient.NotificationUserFeignClient;
import com.activation.activationuser.responsebody.ExceptionResponseBody;
import com.activation.activationuser.responsebody.SuccessResponseBody;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static com.activation.activationuser.util.MockUser.mockUser;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ConsoleControllerTest {

    @Mock
    private NotificationUserFeignClient feignClient;

    @InjectMocks
    private ConsoleController notificationController;


    private final UserDTO user = mockUser();


    @Test
    void notifyUserByConsole_Test(){
        when(feignClient.notifyUserByConsole(user)).thenReturn(new ResponseEntity<>(new SuccessResponseBody(), HttpStatus.OK));
        notificationController.notifyUser(user);

        verify(feignClient).notifyUserByConsole(user);
    }

    @Test
    void notifyUserByConsole_returnExceptionResponse_Test(){
        when(feignClient.notifyUserByConsole(user)).thenReturn(new ResponseEntity<>(
                new ExceptionResponseBody(),
                HttpStatus.INTERNAL_SERVER_ERROR));

        notificationController.notifyUser(user);

        verify(feignClient).notifyUserByConsole(user);
    }
}

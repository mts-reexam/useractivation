package com.activation.activationuser.controller;

import com.activation.activationuser.dto.UserEmailDTO;
import com.activation.activationuser.exception.NoUserInDbException;
import com.activation.activationuser.exception.UserAlreadyActivatedException;
import com.activation.activationuser.responsebody.ExceptionResponseBody;
import com.activation.activationuser.responsebody.SuccessResponseBody;
import com.activation.activationuser.service.activationservice.ActivateUserServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static com.activation.activationuser.util.AsJsonString.asJsonString;
import static com.activation.activationuser.util.MockUser.mockUserEmail;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(ActivationController.class)
public class ActivationControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ActivateUserServiceImpl service;



    UserEmailDTO email;

    @BeforeEach
    void init(){
        email = mockUserEmail();
    }

    @Test
    void activateUser_returnSuccessfulResponse_Test() throws Exception {


        doReturn(new SuccessResponseBody()).when(service).activateUser(any(UserEmailDTO.class));
        mockMvc.perform(put("/activation/v0/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(email)))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$.status").value("OK"))
                .andExpect(jsonPath("$.trace.message").value(
                        "Успешный запрос на активацию"
                ));

        verify(service, times(1)).activateUser(any(UserEmailDTO.class));
    }

    @Test
    void activateUser_returnExceptionResponse_UserAlreadyActivated_Test() throws Exception {

        doReturn(new ExceptionResponseBody(new UserAlreadyActivatedException(""))).when(service).activateUser(any(UserEmailDTO.class));
        mockMvc.perform(put("/activation/v0/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(email)))
                .andExpect(status().is(409))
                .andExpect(jsonPath("$.status").value("CONFLICT"))
                .andExpect(jsonPath("$.trace.message").value(
                        "Пользователь с таким адресом электронной почты () уже активирован!"
                ));

        verify(service).activateUser(any(UserEmailDTO.class));
    }

    @Test
    void activateUser_returnExceptionResponse_NoUserInDbException_Test() throws Exception {

        doReturn(new ExceptionResponseBody(new NoUserInDbException(""))).when(service).activateUser(any(UserEmailDTO.class));
        mockMvc.perform(put("/activation/v0/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(email)))
                .andExpect(status().is(404))
                .andExpect(jsonPath("$.status").value("NOT_FOUND"))
                .andExpect(jsonPath("$.trace.message").value(
                        "Пользователь с таким адресом электронной почты () не найден"));

        verify(service).activateUser(any(UserEmailDTO.class));
    }






}

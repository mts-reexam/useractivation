package com.activation.activationuser.util;

import com.activation.activationuser.dto.UserDTO;
import com.activation.activationuser.dto.UserEmailDTO;
import com.activation.activationuser.entity.UserEntity;
import com.activation.activationuser.entity.UserNotifyEntity;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class MockUser {


    private static final String EMAIL = "user@gmail.com";

    static UserEntity userEntity = new UserEntity();

    static UserEmailDTO userEmailDTO = new UserEmailDTO(EMAIL);


    public static UserNotifyEntity mockUserNotifyEntity(){
        UserDTO user = mockUser();
        return new UserNotifyEntity(user.getEmail(),user.getUsername());
    }

    public static UserDTO mockUser(){
        return new UserDTO("user", EMAIL);
    }

    public static UserEmailDTO mockUserEmail() {
        return userEmailDTO;
    }

    public static UserEntity mockUserEntity() throws IllegalAccessException {
        UserDTO user = mockUser();
        for (Field field : userEntity.getClass().getDeclaredFields()){
            field.setAccessible(true);
            switch (field.getName()){
                case "username" -> field.set(userEntity,user.getUsername());
                case "email" -> field.set(userEntity,user.getEmail());
                case "activated" -> field.set(userEntity,false);
            }
            field.setAccessible(false);
        }
        return userEntity;
    }

    public static ArrayList<UserNotifyEntity> fillList(UserNotifyEntity entity){
        return new ArrayList<>(List.of(
                entity, entity,
                entity, entity,
                entity, entity,
                entity, entity,
                entity, entity));
    }



}

package com.activation.activationuser.util;

import com.fasterxml.jackson.databind.ObjectMapper;

public class AsJsonString {

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
